Source: liblatex-tounicode-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Andrius Merkys <merkys@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: help2man,
                     perl
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/liblatex-tounicode-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/liblatex-tounicode-perl.git
Homepage: https://metacpan.org/release/LaTeX-ToUnicode
Rules-Requires-Root: no

Package: liblatex-tounicode-perl
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${perl:Depends}
Breaks: texlive-bibtex-extra (<< 2023.20231207-2)
Replaces: texlive-bibtex-extra (<< 2023.20231207-2)
Description: Convert LaTeX commands to Unicode
 LaTeX::ToUnicode provides a method to convert LaTeX-style markups for accents
 etc. into their Unicode equivalents. It translates commands for special
 characters or accents into their Unicode equivalents and removes formatting
 commands. It is not at all bulletproof or complete.
 .
 This module converts values from BibTeX files into plain text.
 .
 In contrast to TeX::Encode, this module does not create HTML of any kind,
 including for HTML/XML metacharacters such as <, >, &, which can appear
 literally in the output. Entities are other handling for these has to happen
 at another level, if need be.
